## Marco's dotfiles

![banner](dotfiles-banner.png)

## Table of contents 
A list of applications I use and their configuration files.  

[Vim](#vim)  
[i3 Window Manager](#i3-window-manager)  
[i3 Blocks](#i3-blocks)  
[Alacritty](#alacritty)  
[Picom](#picom)  
[Nitrogen](#nitrogen)  
[Xinitrc](#xinitrc)  
[Xprofile](#xprofile)  

## VIM  
Vim is my main text editor, I use it for editing all my markdown files, configuration files and for web development.  

These are my must have plugins, which I use all the time:
- **[vim-plug](https://github.com/junegunn/vim-plug)**, a minimalist vim plugin manager. I use this to install and manage all my vim plugins.  
- **[fzf.vim](https://github.com/junegunn/fzf.vim)**, a fuzzy finder in your vim to search for files, super fast!
- **[ale](https://github.com/dense-analysis/ale)**, checks syntax in vim asynchronously and fixes files with LSP support.  
- **[vim-polyglot](sheerun/vim-polyglot)**, a solid language pack for vim.  
- **[indentLine](https://github.com/Yggdroot/indentLine)**, a vim plugin to display the indention levels with thin vertical lines. 
- **[gruvbox](https://github.com/morhetz/gruvbox)**, the best color scheme!
- **[papercolor-theme](https://github.com/NLKNguyen/papercolor-theme)**, my favourite light color scheme.  
- **[markdown-preview.nvim](https://github.com/iamcco/markdown-preview.nvim)**, a markdown preview plugin that you must have, if you enjoy working on your markdown files with vim. You'll be able to preview markdown on your browser with synchronized scrolling and flexible configuration.  

## i3 Window Manager 
[i3](https://i3wm.org/) is a dynamic tilling window manager. It's easy to install, configure and use. It has made working on my computer productive and fun. I use [i3 gaps](https://github.com/Airblader/i3), as I like the space between my windows containers. 

## i3 Blocks  
[i3 blocks](https://github.com/vivien/i3blocks) is a feed generator for text based status bars. It executes your command lines and generates a status line from their output. Check out my scripts that I use for this.  

## Alacritty  
[Alacritty](https://github.com/alacritty/alacritty) is a modern terminal emulator that allows for extensive configuration and high performance.

## Picom  
[Picom](https://github.com/yshui/picom) is a compositor for X. Picom helps us customize our window appearances and behaviour. It's what you'll need to have transparency, blur, shadows and animations - which adds to the aesthetic feel of the desktop. Also fixes screen tearing issues ;)

## Nitrogen  
[Nitrogen](https://wiki.archlinux.org/title/nitrogen), is a fast and lightweight (GTK2) desktop background browser and setter for X Window. This is what I use to configure my wallpapers on all my screens.  

## Xinitrc  
[xinitrc](https://wiki.archlinux.org/title/Xinit) program allows a user to manually start Xorg display server. The startx script is a front-end for xinit. Some advise from my experience, copy the '/etc/X11/xinit/xinitrc' and use that to add your edits, because there are important scripts that need to run by default.

## Xprofile  
[xprofile](https://wiki.archlinux.org/title/xprofile), allows you to execute commands at the beginning of the X user session, before the window manager is started. I noticed that if a start my programs from this file rather than the xinitrc, I don't get programs loading in the background once I'm logged in. 


